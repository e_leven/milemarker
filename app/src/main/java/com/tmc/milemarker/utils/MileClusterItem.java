package com.tmc.milemarker.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.tmc.milemarker.models.MileMarker;

/**
 * Created by Eugene Levenetc on 15/02/2016.
 */
public class MileClusterItem implements ClusterItem {
	private final LatLng position;
	private MileMarker mileMarker;

	public MileClusterItem(MileMarker mileMarker) {
		this.mileMarker = mileMarker;
		position = new LatLng(mileMarker.getLat(), mileMarker.getLon());
	}

	public MileMarker getMileMarker() {
		return mileMarker;
	}

	@Override public LatLng getPosition() {
		return position;
	}

	@Override public String toString() {
		return "MileClusterItem{" +
				"mileMarker=" + mileMarker +
				'}';
	}

	@Override public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MileClusterItem that = (MileClusterItem) o;

		if (!position.equals(that.position)) return false;
		return mileMarker.equals(that.mileMarker);

	}

	@Override public int hashCode() {
		int result = position.hashCode();
		result = 31 * result + mileMarker.hashCode();
		return result;
	}
}
