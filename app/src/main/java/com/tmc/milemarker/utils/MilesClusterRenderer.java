package com.tmc.milemarker.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.tmc.milemarker.BuildConfig;
import com.tmc.milemarker.R;
import com.tmc.milemarker.cache.IMarkerBitmapsCache;
import com.tmc.milemarker.models.MileMarker;
import com.tmc.milemarker.models.Route;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Eugene Levenetc on 15/02/2016.
 */
public class MilesClusterRenderer extends DefaultClusterRenderer<MileClusterItem> implements GoogleMap.OnCameraChangeListener {

	private Holder viewMarkerHolder;
	private Context context;
	private GoogleMap map;
	private ClusterManager<MileClusterItem> clusterManager;
	private IMarkerBitmapsCache cache;
	private HashSet<MileClusterItem> visible = new HashSet<>();
	private HashSet<MileClusterItem> invisible = new HashSet<>();
	private Route route;

	public MilesClusterRenderer(
			Context context,
			GoogleMap map,
			final ClusterManager<MileClusterItem> clusterManager,
			IMarkerBitmapsCache cache
	) {
		super(context, map, clusterManager);
		this.context = context;
		this.map = map;
		this.clusterManager = clusterManager;
		this.cache = cache;
	}

	public void clearItems() {
		visible.clear();
		invisible.clear();
	}

	@Override protected void onBeforeClusterRendered(Cluster<MileClusterItem> cluster, MarkerOptions markerOptions) {
		super.onBeforeClusterRendered(cluster, markerOptions);
	}

	@Override protected void onBeforeClusterItemRendered(MileClusterItem clusterItem, MarkerOptions markerOptions) {

		final LatLngBounds mLatLngBounds = map.getProjection().getVisibleRegion().latLngBounds;
		final MileMarker mileMarker = clusterItem.getMileMarker();

		if (mLatLngBounds.contains(mileMarker.getLocation())) {
			drawMarker(clusterItem, markerOptions, mileMarker);
		} else {
			invisible.add(clusterItem);
		}
	}

	@Override protected boolean shouldRenderAsCluster(Cluster<MileClusterItem> cluster) {
		return cluster.getSize() > 3;
	}

	@Override public void onCameraChange(CameraPosition cameraPosition) {

		final LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
		boolean changed = false;

		Iterator<MileClusterItem> visibleIterator = visible.iterator();
		while (visibleIterator.hasNext()) {
			MileClusterItem next = visibleIterator.next();
			if (!bounds.contains(next.getPosition())) {
				visibleIterator.remove();
				clusterManager.removeItem(next);
				invisible.add(next);
				changed = true;
			}
		}

		Iterator<MileClusterItem> invisibleIterator = invisible.iterator();
		while (invisibleIterator.hasNext()) {
			MileClusterItem next = invisibleIterator.next();
			if (bounds.contains(next.getPosition())) {
				invisibleIterator.remove();
				clusterManager.addItem(next);
				visible.add(next);

				Marker marker = getMarker(next);
				if (marker != null) {//if marker already created then update bitmap
					Bitmap markerBitmap = createAndCacheBitmap(next.getMileMarker());
					marker.setIcon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
				}

				changed = true;
			}
		}

		if (changed) clusterManager.cluster();
	}

	private void drawMarker(MileClusterItem clusterItem, MarkerOptions markerOptions, MileMarker mileMarker) {
		Bitmap markerBitmap = createAndCacheBitmap(mileMarker);
		markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
		markerOptions.visible(true);
		visible.add(clusterItem);
	}

	public void addInvisible(List<MileClusterItem> clusterItems) {
		invisible.addAll(clusterItems);
	}

	private Bitmap createAndCacheBitmap(MileMarker mileMarker) {

		boolean recycled = false;
		Bitmap cached = cache.getMarkerBitmap(mileMarker);
		if (cached != null && cached.isRecycled()) {
			recycled = true;
			cached = null;
		}
		if (cached == null) {
			if (BuildConfig.DEBUG)
				Log.i("bitmap", "creating new: " + mileMarker.getId() + " was recycled:" + recycled);
			if (viewMarkerHolder == null) {
				viewMarkerHolder = new Holder(LayoutInflater.from(context).inflate(R.layout.view_milemarker, null));
				viewMarkerHolder.view.setDrawingCacheEnabled(true);
			}

			final String direction = route.getDirection();
			final String distance = String.valueOf(mileMarker.getDistance());
			final String name = route.getName();

			viewMarkerHolder.textDistance.setText(distance);
			viewMarkerHolder.textRouteName.setText(name);

			if (direction == null) {
				viewMarkerHolder.textDirection.setVisibility(View.GONE);
			} else {
				viewMarkerHolder.textDirection.setVisibility(View.VISIBLE);
				viewMarkerHolder.textDirection.setText(direction);
			}

			View view = viewMarkerHolder.view;
			view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
			view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
			view.destroyDrawingCache();
			view.buildDrawingCache();

			return cache.putMarkerBitmap(mileMarker, view.getDrawingCache());
		} else {
			if (BuildConfig.DEBUG) Log.i("bitmap", "cached: " + mileMarker.getId());
			return cached;
		}
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public static class Holder {

		@Bind(R.id.text_direction) TextView textDirection;
		@Bind(R.id.text_distance) TextView textDistance;
		@Bind(R.id.text_route_name) TextView textRouteName;
		private View view;

		public Holder(View view) {
			this.view = view;
			ButterKnife.bind(this, view);
		}
	}
}
