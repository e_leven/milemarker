package com.tmc.milemarker.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

import com.tmc.milemarker.enums.DistUnit;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class Utils {

	private static final int LOCATION_REQUEST = 100;

	public static double distance(double latA, double lonA, double latB, double lonB) {
		return distance(latA, lonA, latB, lonB, DistUnit.KIL);
	}

	public static double distance(double latA, double lonA, double latB, double lonB, DistUnit unit) {
		if (latA == -1 || lonA == -1) return 0;
		final double theta = lonA - lonB;
		double dist = sin(toRadians(latA)) * sin(toRadians(latB)) + cos(toRadians(latA)) * cos(toRadians(latB)) * cos(toRadians(theta));
		dist = Math.acos(dist);
		dist = toDegrees(dist);
		dist = dist * 60 * 1.1515;
		if (unit == DistUnit.KIL) dist = dist * 1.609344;
		else if (unit == DistUnit.MIL) dist = dist * 0.8684;

		return dist;
	}

	public static boolean checkLocationPermission(Fragment fragment, boolean request) {
		if (ActivityCompat.checkSelfPermission(
				fragment.getContext(),
				Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fragment.getContext(),
				Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			if (request)
				fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
			return false;
		} else {
			return true;
		}
	}

	public static boolean isLocationPermissionGranted(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case LOCATION_REQUEST: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isMarshmallow() {
		return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
	}
}
