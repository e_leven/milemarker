package com.tmc.milemarker.activites;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.tmc.milemarker.R;
import com.tmc.milemarker.fragments.MapFragment;

public class MileMarkerActivity extends FragmentActivity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.root_container, MapFragment.create())
					.commit();
		}
	}
}
