package com.tmc.milemarker.parsers;

import com.tmc.milemarker.models.MileMarker;
import com.tmc.milemarker.models.Route;
import com.tmc.milemarker.models.RouteParams;
import com.tmc.milemarker.models.RouteVertex;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public interface IGpxXmlRouteParser {
	Route parse(RouteParams routeParams) throws XmlPullParserException, NumberFormatException, IOException;

	/**
	 * @return length of route
	 */
	double parse(String routeId, InputStream in, List<RouteVertex> vertices, List<MileMarker> markers) throws XmlPullParserException, IOException;
}
