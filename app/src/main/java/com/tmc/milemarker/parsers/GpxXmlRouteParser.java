package com.tmc.milemarker.parsers;

import android.content.Context;
import android.support.annotation.RawRes;
import android.util.Xml;

import com.tmc.milemarker.models.MileMarker;
import com.tmc.milemarker.models.Route;
import com.tmc.milemarker.models.RouteParams;
import com.tmc.milemarker.models.RouteVertex;
import com.tmc.milemarker.utils.Utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class GpxXmlRouteParser implements IGpxXmlRouteParser {

	private Context context;

	public GpxXmlRouteParser(Context context) {
		this.context = context;
	}

	@Override public Route parse(RouteParams routeParams) throws XmlPullParserException, IOException {
		InputStream in = null;
		List<RouteVertex> vertices = new ArrayList<>();
		List<MileMarker> markers = new ArrayList<>();
		double totalDistance = 0;
		try {
			in = context.getResources().openRawResource(routeParams.getRawResId());
			totalDistance = parse(routeParams.getId(), in, vertices, markers);
		} finally {
			if (in != null) in.close();
		}

		return new Route(vertices, markers, totalDistance, routeParams);
	}

	@Override public double parse(
			String routeId,
			InputStream in,
			List<RouteVertex> vertices,
			List<MileMarker> markers) throws XmlPullParserException, IOException {

		double totalDistance = 0;
		XmlPullParser parser = Xml.newPullParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		parser.setInput(in, null);
		parser.nextTag();

		parser.require(XmlPullParser.START_TAG, null, "gpx");
		int next = parser.next();
		RouteVertex prevVertex = null;
		double lastVertexDist = 0;
		double lastMarkerDist = 0;
		while (next != XmlPullParser.END_DOCUMENT) {

			next = parser.next();

			if (next == XmlPullParser.START_TAG) {
				if ("wpt".equals(parser.getName())) {
					final double lat = Double.parseDouble(parser.getAttributeValue(null, "lat"));
					final double lon = Double.parseDouble(parser.getAttributeValue(null, "lon"));
					String title = null;

					if (parser.nextToken() == XmlPullParser.TEXT) {
						next = parser.next();
						title = parser.nextText();
					}

					RouteVertex vertex = new RouteVertex(lat, lon, title);

					if (prevVertex != null) {
						double latA = prevVertex.getLat();
						double lonA = prevVertex.getLon();
						double latB = vertex.getLat();
						double lonB = vertex.getLon();
						double nextDistance = Utils.distance(latA, lonA, latB, lonB);
						totalDistance += nextDistance;

						double iter = -lastVertexDist;
						if (lastVertexDist + nextDistance >= 0.1) {
							while (iter + 0.1 <= lastVertexDist + nextDistance) {
								iter += 0.1;
								lastMarkerDist += 0.1;
								double prog = Math.abs(iter / (nextDistance));//0.0 - 1.0

								double latMarker = latA + (latB - latA) * prog;
								double lonMarker = lonA + (lonB - lonA) * prog;

								lastMarkerDist = lastMarkerDist * 10;
								lastMarkerDist = Math.round(lastMarkerDist);
								lastMarkerDist = lastMarkerDist / 10;

								markers.add(new MileMarker(routeId + lastMarkerDist, "", latMarker, lonMarker, lastMarkerDist, null, null, null));
								lastVertexDist = 0;
							}
						}
						if (iter == 0) {
							lastVertexDist = nextDistance;
						} else {
							lastVertexDist = nextDistance - iter;
						}
					}

					prevVertex = vertex;
					vertices.add(vertex);
				}
			}
		}
		return totalDistance;
	}
}