package com.tmc.milemarker.models;

import android.support.annotation.Nullable;
import android.support.annotation.RawRes;

/**
 * Created by Eugene Levenetc on 16/02/2016.
 */
public class RouteParams {

	private final String name;
	private final String id;
	private final @Nullable String direction;
	private int rawResId;

	public RouteParams(String id, String name, @Nullable String direction, @RawRes int rawResId) {
		this.id = id;
		this.name = name;
		this.direction = direction;
		this.rawResId = rawResId;
	}

	public String getId() {
		return id;
	}

	@Nullable public String getDirection() {
		return direction;
	}

	public String getName() {
		return name;
	}

	public int getRawResId() {
		return rawResId;
	}
}
