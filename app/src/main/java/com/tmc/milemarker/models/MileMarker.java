package com.tmc.milemarker.models;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.tmc.milemarker.enums.Direction;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class MileMarker {

	private final String id;
	private final String routeId;
	private final String optValues;
	private final double distance;
	private final double lat;
	private final double lon;
	private final @Nullable String title;
	private final Direction direction;
	private final LatLng location;

	public MileMarker(
			String id,
			String routeId,
			double lat,
			double lon,
			double distance,
			Direction direction,
			@Nullable String title,
			@Nullable String optValues
	) {
		this.id = id;
		this.routeId = routeId;
		this.lat = lat;
		this.lon = lon;
		this.title = title;
		this.direction = direction;
		this.distance = distance;
		this.optValues = optValues;
		location = new LatLng(lat, lon);
	}

	public LatLng getLocation() {
		return location;
	}

	public String getId() {
		return id;
	}

	public double getDistance() {
		return distance;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	@Nullable public String getTitle() {
		return title;
	}

	@Override public String toString() {
		return "RouteVertex{" +
				"lat=" + lat +
				", lon=" + lon +
				", distance='" + distance + '\'' +
				'}';
	}

	@Override public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MileMarker that = (MileMarker) o;

		return id.equals(that.id);

	}

	@Override public int hashCode() {
		return id.hashCode();
	}
}