package com.tmc.milemarker.models;

import android.support.annotation.Nullable;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class RouteVertex {

	private final double lat;
	private final double lon;
	private final @Nullable String title;

	public RouteVertex(double lat, double lon, @Nullable String title) {
		this.lat = lat;
		this.lon = lon;
		this.title = title;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	@Nullable public String getTitle() {
		return title;
	}

	@Override public String toString() {
		return "RouteVertex{" +
				"lat=" + lat +
				", lon=" + lon +
				", title='" + title + '\'' +
				'}';
	}
}