package com.tmc.milemarker.models;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class Route {

	private final List<RouteVertex> routeVertices;
	private final List<MileMarker> markers;
	private final double distance;
	private RouteParams routeParams;

	public Route(
			List<RouteVertex> routeVertices,
			List<MileMarker> markers,
			double distance,
			RouteParams routeParams) {
		this.routeVertices = routeVertices;
		this.markers = markers;
		this.distance = distance;
		this.routeParams = routeParams;
	}

	public double getDistance() {
		return distance;
	}

	public String getName() {
		return routeParams.getName();
	}

	public @Nullable String getDirection() {
		return routeParams.getDirection();
	}

	public List<MileMarker> getMarkers() {
		return markers;
	}

	public String getId() {
		return routeParams.getId();
	}

	public List<RouteVertex> getRouteVertices() {
		return routeVertices;
	}

	@Override public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Route route = (Route) o;

		return routeParams.getId().equals(route.routeParams.getName());

	}

	@Override public int hashCode() {
		return routeParams.getId().hashCode();
	}
}
