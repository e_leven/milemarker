package com.tmc.milemarker.repository;

import android.content.Context;

import com.tmc.milemarker.R;
import com.tmc.milemarker.models.Route;
import com.tmc.milemarker.models.RouteParams;
import com.tmc.milemarker.parsers.GpxXmlRouteParser;
import com.tmc.milemarker.parsers.IGpxXmlRouteParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class DataRepository implements IDataRepository {

	private IGpxXmlRouteParser routeParser;

	private List<RouteParams> routes = new LinkedList<RouteParams>() {{
		add(new RouteParams("a2li.gpx", "A2", "Li", R.raw.a2li));
		add(new RouteParams("a2litoa9re.gpx", "A2", "Li", R.raw.a2litoa9re));
		add(new RouteParams("a2re.gpx", "A2", "Re", R.raw.a2re));
		add(new RouteParams("a2relong.gpx", "A2", "Re", R.raw.a2relong));
		add(new RouteParams("a2retest.gpx", "A2", "Re", R.raw.a2retest));
		add(new RouteParams("a12.gpx", "A12", null, R.raw.a12));
		add(new RouteParams("afslag.gpx", "A", null, R.raw.afslag));
		add(new RouteParams("n402li.gpx", "N4", "Li", R.raw.n402li));
		add(new RouteParams("n402re.gpx", "N4", "Re", R.raw.n402re));
		add(new RouteParams("n402re.gpx", "N4", "Re", R.raw.n419li));
		add(new RouteParams("oprit.gpx", "OP", null, R.raw.oprit));
	}};


	public DataRepository(Context context) {
		routeParser = new GpxXmlRouteParser(context);
	}

	@Override public Observable<List<Route>> loadRoutes() {
		return Observable.create(new Observable.OnSubscribe<List<Route>>() {
			@Override public void call(Subscriber<? super List<Route>> subscriber) {
				try {
					subscriber.onNext(loadRoutesInternal());
					subscriber.onCompleted();
				} catch (IOException | XmlPullParserException e) {
					//log error
					e.printStackTrace();
					subscriber.onError(e);
				}

			}
		});
	}

	private List<Route> loadRoutesInternal() throws IOException, XmlPullParserException {

		List<Route> result = new LinkedList<>();
		for (RouteParams params : routes) result.add(routeParser.parse(params));
		return result;
	}
}
