package com.tmc.milemarker.repository;

import android.support.annotation.RawRes;

import com.tmc.milemarker.models.Route;

import java.util.List;

import rx.Observable;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public interface IDataRepository {
	Observable<List<Route>> loadRoutes();
}
