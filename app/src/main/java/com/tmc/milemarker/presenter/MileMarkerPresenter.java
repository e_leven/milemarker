package com.tmc.milemarker.presenter;

import com.tmc.milemarker.models.Route;
import com.tmc.milemarker.repository.IDataRepository;
import com.tmc.milemarker.views.IMileMarkerView;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public class MileMarkerPresenter {

	private IDataRepository dataRepository;
	private IMileMarkerView view;

	public MileMarkerPresenter(IDataRepository dataRepository) {
		this.dataRepository = dataRepository;
	}

	public void onViewReady() {
		dataRepository
				.loadRoutes()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<List<Route>>() {
					@Override public void call(List<Route> routes) {
						view.setData(routes);
					}
				}, new Action1<Throwable>() {
					@Override public void call(Throwable throwable) {
						view.dataLoadingErrorHandler();
					}
				});
	}

	public void setView(IMileMarkerView view) {
		this.view = view;
	}
}