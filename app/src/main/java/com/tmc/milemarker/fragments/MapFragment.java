package com.tmc.milemarker.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.CustomNonHierarchicalDistanceBasedAlgorithm;
import com.tmc.milemarker.R;
import com.tmc.milemarker.cache.IMarkerBitmapsCache;
import com.tmc.milemarker.cache.MarkerBitmapCache;
import com.tmc.milemarker.models.MileMarker;
import com.tmc.milemarker.models.Route;
import com.tmc.milemarker.presenter.MileMarkerPresenter;
import com.tmc.milemarker.repository.DataRepository;
import com.tmc.milemarker.utils.MileClusterItem;
import com.tmc.milemarker.utils.MilesClusterRenderer;
import com.tmc.milemarker.utils.Utils;
import com.tmc.milemarker.views.IMileMarkerView;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Eugene Levenetc on 16/02/2016.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, IMileMarkerView, AdapterView.OnItemSelectedListener {

	@Bind(R.id.map_view) MapView mapView;
	@Bind(R.id.spinner_view) AppCompatSpinner spinner;
	@Bind(R.id.text_state) TextView textState;

	private IMarkerBitmapsCache bitmapCache = MarkerBitmapCache.createInst();
	private MileMarkerPresenter presenter;
	private GoogleMap map;
	private ClusterManager<MileClusterItem> clusterManager;
	private List<Route> routes;
	private MilesClusterRenderer clusterRenderer;

	public static MapFragment create() {
		return new MapFragment();
	}

	@Override public void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	@Override public void onPause() {
		super.onPause();
		mapView.onPause();
	}

	@Override public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		ButterKnife.unbind(this);
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		presenter = new MileMarkerPresenter(new DataRepository(getContext()));
		presenter.setView(this);
	}

	@Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View result = inflater.inflate(R.layout.fragment_map, container, false);
		ButterKnife.bind(this, result);

		spinner.setVisibility(View.GONE);
		spinner.setOnItemSelectedListener(this);

		mapView.onCreate(savedInstanceState);
		mapView.getMapAsync(this);

		textState.setText(R.string.loading);

		return result;
	}

	@Override public void onMapReady(GoogleMap googleMap) {

		map = googleMap;

		clusterManager = new ClusterManager<>(getContext(), map);
		clusterRenderer = new MilesClusterRenderer(getContext(), map, clusterManager, bitmapCache);
		clusterManager.setRenderer(clusterRenderer);
		clusterManager.setAlgorithm(new CustomNonHierarchicalDistanceBasedAlgorithm<MileClusterItem>());

		if (Utils.checkLocationPermission(this, true)) {
			map.setMyLocationEnabled(true);
			map.getUiSettings().setMyLocationButtonEnabled(true);
		}
		map.setOnCameraChangeListener(clusterManager);
		map.setOnMarkerClickListener(clusterManager);

		presenter.onViewReady();
	}

	@Override public void setData(List<Route> routes) {
		this.routes = routes;
		textState.setVisibility(View.GONE);
		spinner.setVisibility(View.VISIBLE);

		ArrayAdapter<Object> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		for (Route route : routes) adapter.add(route.getId());
		spinner.setAdapter(adapter);
	}

	@Override public void dataLoadingErrorHandler() {
		textState.setText(R.string.default_error_message);
	}

	@SuppressWarnings("ConstantConditions")
	@Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		final Route route = routes.get(position);
		addRouteItems(route);
		zoomToRoute(route);
	}

	@Override public void onNothingSelected(AdapterView<?> parent) {

	}

	private void addRouteItems(Route route) {

		clusterRenderer.setRoute(route);

		if (!isMapEmpty()) {
			clusterRenderer.clearItems();
			clusterManager.clearItems();
		}

		List<MileClusterItem> clusterItems = new LinkedList<>();
		for (MileMarker mileMarker : route.getMarkers())
			clusterItems.add(new MileClusterItem(mileMarker));

		clusterRenderer.addInvisible(clusterItems);
		clusterManager.addItems(clusterItems);
	}

	private boolean isMapEmpty() {
		return clusterManager.getMarkerCollection().getMarkers().size() == 0;
	}

	private void zoomToRoute(Route route) {
		MileMarker routeVertex = route.getMarkers().get(0);
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(routeVertex.getLocation())
				.zoom(17)
				.build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	@Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (Utils.checkLocationPermission(this, false)) {
			map.setMyLocationEnabled(true);
			map.getUiSettings().setMyLocationButtonEnabled(true);
		}
	}
}
