package com.tmc.milemarker.views;

import com.tmc.milemarker.models.Route;

import java.util.List;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public interface IMileMarkerView {
	void setData(List<Route> routes);

	void dataLoadingErrorHandler();
}
