package com.tmc.milemarker.cache;

import android.graphics.Bitmap;

import com.tmc.milemarker.models.MileMarker;

/**
 * Created by Eugene Levenetc on 15/02/2016.
 */
public interface IMarkerBitmapsCache {
	Bitmap getMarkerBitmap(MileMarker marker);
	Bitmap putMarkerBitmap(MileMarker marker, Bitmap bitmap);
}
