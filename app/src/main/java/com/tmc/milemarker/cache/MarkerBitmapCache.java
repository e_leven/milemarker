package com.tmc.milemarker.cache;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.tmc.milemarker.models.MileMarker;

/**
 * Created by Eugene Levenetc on 15/02/2016.
 */
public class MarkerBitmapCache extends LruCache<String, Bitmap> implements IMarkerBitmapsCache {

	private static MarkerBitmapCache inst;

	public static MarkerBitmapCache createInst() {
		if (inst == null) {
			int maxSize = (int) (Runtime.getRuntime().maxMemory() / 1024);
			inst = new MarkerBitmapCache(maxSize);
		}
		return inst;
	}

	private MarkerBitmapCache(int maxSize) {
		super(maxSize);
	}

	@Override protected int sizeOf(String key, Bitmap bitmap) {
		return bitmap.getByteCount() / 1024;
	}

	@Override public Bitmap getMarkerBitmap(MileMarker marker) {
		return get(marker.getId());
	}

	@Override public Bitmap putMarkerBitmap(MileMarker marker, Bitmap bitmap) {
		put(marker.getId(), bitmap);
		return bitmap;
	}
}
