package com.tmc.milemarker.enums;

/**
 * Created by Eugene Levenetc on 14/02/2016.
 */
public enum DistUnit {
	KIL, MIL
}
