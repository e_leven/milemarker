package com.tmc.milemarker.enums;

/**
 * Created by Eugene Levenetc on 15/02/2016.
 */
public enum Direction {
	Li, Ri
}
