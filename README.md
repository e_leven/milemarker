
# Core classes of the project #
* **MapFragment** which implements **IMileMarkerView** interface and aimed to initialise **GoogleMap** instance and depict loaded routes
* **MileMarkerPresenter** is reposible for data loading from **DataRepository**
* **DataRepository** is responsible for retriving data and parsing data. It implements IDataRepository interface so it could be potentially replaced with any other source of data (Rest API, File system).
* **GpxXmlRouteParser** aimed to parse xml files from raw directory of the app
* **MilesClusterRenderer** handles render of markers.